package com.example.demo.util;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Enumeration;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class HttpServletUtil {
    /**
     * @Title: getParameter
     * @Description: 获取请求参数Parameter
     * @param request
     * @return
     */
    public static Map<String, String> getParameter(HttpServletRequest request) {
        SortedMap<String, String> params =  new TreeMap<>();
        Enumeration<String> attrNames = request.getParameterNames();
        while (attrNames.hasMoreElements()) {
            String name = attrNames.nextElement();
            params.put(name, request.getParameter(name));
        }
        return params;
    }
}
