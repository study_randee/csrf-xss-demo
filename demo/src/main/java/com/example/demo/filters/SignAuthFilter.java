package com.example.demo.filters;



import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: zjhang
 * @Date: 2021/6/19 14:37
 * @Description: 拦截防止注入漏洞（即防止XSS的跨站脚本攻击）
 */
@WebFilter(urlPatterns = "/*", filterName = "signAuthFilter")
public class SignAuthFilter implements Filter {

    private FilterConfig filterConfig = null;

    private Boolean isRequireSign = null;

//    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
//            Arrays.asList("/main/excludefilter", "/login", "/logout", "/register")));

    /**
     * 放开的接口列表
     */
    private static final List<String> ALLOWED_PATHS = Arrays.asList("/api/v1/file/upload");

    public SignAuthFilter(Boolean isRequireSign)
    {
        this.isRequireSign = isRequireSign;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
        if (ALLOWED_PATHS.contains(path)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            filterChain.doFilter(new SignAuthServletRequestWrapper((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, isRequireSign), servletResponse);
        }
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }
}