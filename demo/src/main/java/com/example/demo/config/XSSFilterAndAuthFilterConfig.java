package com.example.demo.config;


import com.example.demo.filters.SignAuthFilter;
import com.example.demo.filters.XssFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class XSSFilterAndAuthFilterConfig {

    /**
     * 配置是否强制开启sign验证
     */
    @Value("${config.isRequireSign:false}")
    private Boolean isRequireSign;

    /**
     * 注册xxs过滤器
     * @return
     */
    @Bean
    public FilterRegistrationBean xxsFilterRegistration(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new XssFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setName("XssFilter");
        filterRegistrationBean.setOrder(99);
        return filterRegistrationBean;
    }

    /**
     * 注册sign过滤器
     * @return
     */
    @Bean
    public FilterRegistrationBean authSignFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new SignAuthFilter(isRequireSign));
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setName("SignAuthFilter");
        filterRegistrationBean.setOrder(98);
        return filterRegistrationBean;
    }
}
