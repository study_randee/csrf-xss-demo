function addHeader(key,value){
    pm.request.headers.remove(key)
    pm.request.headers.add({
        key: key,
        value: value
    })
}

function signHandler(params)
{
    let secret = "350cb4303db847788eee5b125f7fdbaa"
    let timestamp = Date.now()


    // // 获取参数对象所有key
    // let paramKeys = []
    // for (let key in params) {
    //     paramKeys.push(key)
    // }
    // // 按字母a-z排序
    // paramKeys.sort()
    // // console.log(paramKeys)
    // let queryStr = ""
    // for (let i in paramKeys) {
    //     let key = paramKeys[i]
    //     let param = params[key]
    //     if (param instanceof Object || param instanceof Array) {
    //         queryStr += key + "=" + JSON.stringify(param) + "&"
    //     } else {
    //         queryStr += key + "=" + param + "&"
    //     }
    // }

    let queryStr = ''
    // console.log(params)

    if(params instanceof Array)
    {
        console.log('buildQueryStrByArray')
        queryStr = buildQueryStrByArray(params)
    }
    else  if(params.instanceof === Object)
    {
        console.log('buildQueryStrByObj')
        queryStr = buildQueryStrByObj(params)
    }
    else{
        throw new Exception('非对象或数组类型')
    }

    // queryStr结尾拼接时间戳和私钥
    queryStr += "timestamp=" + timestamp + "&"
    queryStr += "secret=" + secret
    console.log('queryStr:' + queryStr)
    let sign = CryptoJS.MD5(queryStr).toString().toLowerCase();
    addHeader('Timestamp',timestamp)
    addHeader('Sign',sign)
}

const access_token_name = 'Access-Token'
const csrf_token_name = 'X-XSRF-TOKEN'

var access_token = postman.getEnvironmentVariable("access_token");
var xsrf_token = postman.getEnvironmentVariable("xsrf-token");

// access token
addHeader(access_token_name,access_token)
// csrf token
addHeader(csrf_token_name,xsrf_token)

function getQueryParams()
{
    let query = pm.request.url.query;
    let params = {}
    query.each(item=>{
        console.log(item)
        if(!item.disabled){
            params[item.key] = item.value
        }
    })
    return params;
}

function getRawParams()
{
    // console.log(pm.request.body.raw)
    let params = JSON.parse(pm.request.body.raw)
    return params;
}

function getFormParams()
{
    let params = {}
    let urlencoded = pm.request.body.urlencoded;
    urlencoded.each(item=>{
        if(!item.disabled)
        {
            params[item.key] = item.value
        }
    })
    return params;
}

function objKeySort(obj) {
    var sortedKeys = Object.keys(obj).sort();
    // console.log(sortedKeys)
    //创建一个新的对象，用于存放排好序的键值对
    var newObj = {};
    //遍历sortedKeys数组
    for(var i = 0; i < sortedKeys.length; i++) {
        //向新创建的对象中按照排好的顺序依次增加键值对
        newObj[sortedKeys[i]] = obj[sortedKeys[i]];
    }
    //返回排好序的新对象
    // console.log(newObj)
    return newObj;
}

function buildQueryStrByObj(params)
{
    // 获取参数对象所有key
    let paramKeys = []
    for (let key in params) {
        paramKeys.push(key)
    }

    // 按字母a-z排序
    paramKeys.sort()
    let queryStr = ""
    for (let i in paramKeys) {
        let key = paramKeys[i]
        let param = params[key]
        if (param instanceof Object || param instanceof Array) {
            queryStr += key + "=" + JSON.stringify(param) + "&"
        } else {
            queryStr += key + "=" + param + "&"
        }
    }
    return queryStr
}

function buildQueryStrByArray(paramsArr)
{
    let queryStr = ""

    for(let i= 0; i< paramsArr.length;i++)
    {
        let key = i
        let param = objKeySort(paramsArr[i])
        if (param instanceof Object || param instanceof Array) {
            queryStr += key + "=" + JSON.stringify(param) + "&"
        } else {
            queryStr += key + "=" + param + "&"
        }
    }

    return queryStr
}

// console.log(pm.request.method)
let method = pm.request.method;
let mode = pm.request.body.mode;
console.log(pm.request)
// 暂时只处理GET和POST,POST里的文件上传暂时不处理
if(method == 'GET' || method == 'POST')
{
    try
    {
        let params = null
        if(method == 'GET')
        {
            params = getQueryParams();
        }
        else if(method == 'POST')
        {
            if(mode == 'raw')
            {
                params = getRawParams();
            }
            else if(mode == 'urlencoded')
            {
                // console.log('mode == urlencoded')
                params = getFormParams()
                // console.log(params)
            }
        }

        if(params != null && params != undefined)
        {
            signHandler(params)
        }
    }
    catch(e)
    {
        console.log(e)
    }
}